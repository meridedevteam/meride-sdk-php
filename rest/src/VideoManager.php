<?php
namespace Meride\Rest;

use \Exception;

class VideoManager
{
    public function __construct(RequestManager $requestManager)
    {
        $this->requestManager = $requestManager;

        $this->path = "/rest/video";
    }


    /**
     * Delete by id
     *
     * @param $id
     * @return bool|mixed
     * @throws Exception
     */
    public function delete($id)
    {
        $headers[] = "X-HTTP-Method-Override: DELETE";

        return $this->requestManager->request("POST", $this->path . "/$id.json", false, $headers);
    }


    /**
     * Edit by id
     *
     * @param $id
     * @param $params
     * @return bool|mixed
     * @throws Exception
     */
    public function edit($id, $params)
    {
        $headers[] = "X-HTTP-Method-Override: PUT";

        return $this->requestManager->request("POST", $this->path . "/$id.json", $params, $headers);
    }

    /**
     * Add
     *
     * @param $params
     * @return bool|mixed
     * @throws Exception
     */
    public function add($params)
    {
        if(isset($params["video"]) && is_string($params["video"]) ) {
            $file = new \CurlFile($params["video"]);

            $info = pathinfo($params["video"]);

            $file->setPostFileName(uniqid() . ((isset($info["extension"])) ? "." . $info["extension"] : null));

            $params["video"] = $file;
        }

        if(isset($params["preview_image"]) && is_string($params["preview_image"]) ) {
            $file = new \CurlFile($params["preview_image"]);

            $info = pathinfo($params["preview_image"]);

            $file->setPostFileName(uniqid() . ((isset($info["extension"])) ? "." . $info["extension"] : null));

            $params["preview_image"] = $file;
        }

        return $this->requestManager->request("POST", $this->path . ".json", $params);
    }

    /**
     * Get by id
     *
     * @param $id
     * @return bool|mixed
     * @throws Exception
     */
    public function get($id)
    {
        return $this->requestManager->request("GET", $this->path . "/$id.json");
    }

    /**
     * @param array $params
     * @return bool|mixed
     * @throws Exception
     */
    public function search($params = array())
    {
        return $this->requestManager->request("GET", $this->path . '.json' . (($params) ? ("?" . http_build_query($params)) : ""));
    }


    /**
     * @param $id_video
     * @return bool
     * @throws Exception
     */
    public function forceDeleteVideo($id_video)
    {
        $embedManager = new EmbedManager($this->requestManager);

        $embed_list = $embedManager->search(array('search_video_id' => $id_video));

        if ($embed_list) {
            foreach ($embed_list as $key => $embed) {

                $check = $embedManager->delete($embed->id);

                if (!$check) {
                    return false;
                }
            }
        }

        $check = $this->delete($id_video);

        if (!$check) {
            return false;
        }


        return true;
    }
}