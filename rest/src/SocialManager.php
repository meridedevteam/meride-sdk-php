<?php
namespace Meride\Rest;

use \Exception;

class SocialManager
{
    public function __construct(RequestManager $requestManager)
    {
        $this->requestManager = $requestManager;

        $this->path = "/rest/facebookvideo";
    }


    /**
     * Delete by id
     *
     * @param $id
     * @return bool|mixed
     * @throws Exception
     */
    public function delete($id)
    {
        $headers[] = "X-HTTP-Method-Override: DELETE";

        return $this->requestManager->request("POST", $this->path . "/$id.json", false, $headers);
    }


    /**
     * Add
     *
     * @param $params
     * @return bool|mixed
     * @throws Exception
     */
    public function add($params)
    {
        return $this->requestManager->request("POST", $this->path . ".json", $params);
    }

    /**
     * Get by id
     *
     * @param $id
     * @return bool|mixed
     * @throws Exception
     */
    public function get($id)
    {
        return $this->requestManager->request("GET", $this->path . "/$id.json");
    }

}