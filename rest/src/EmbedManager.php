<?php
namespace Meride\Rest;

use \Exception;


class EmbedManager
{
    public function __construct(RequestManager $requestManager)
    {
        $this->requestManager = $requestManager;

        $this->path = "/rest/embed";
    }


    /**
     * Delete by id
     *
     * @param $id
     * @return bool|mixed
     * @throws Exception
     */
    public function delete($id)
    {
        $headers[] = "X-HTTP-Method-Override: DELETE";

        return $this->requestManager->request("POST", $this->path . "/$id.json", false, $headers);
    }


    /**
     * Edit by id
     *
     * @param $id
     * @param $params
     * @return bool|mixed
     * @throws Exception
     */
    public function edit($id, $params)
    {
        $headers[] = "X-HTTP-Method-Override: PUT";

        return $this->requestManager->request("POST", $this->path . "/$id.json", $params, $headers);
    }

    /**
     * Add
     *
     * @param $params
     * @return bool|mixed
     * @throws Exception
     */
    public function add($params)
    {
        return $this->requestManager->request("POST", $this->path . ".json", $params);
    }

    /**
     * Get by id
     *
     * @param $id
     * @return bool|mixed
     * @throws Exception
     */
    public function get($id)
    {
        return $this->requestManager->request("GET", $this->path . "/$id.json");
    }

    /**
     * @param array $params
     * @return bool|mixed
     * @throws Exception
     */
    public function search($params = array())
    {
        return $this->requestManager->request("GET", $this->path . '.json' . ((count($params)) ? ("?" . http_build_query($params)) : ""));
    }
}