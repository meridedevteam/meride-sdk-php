<?php

namespace Meride\Rest;

use \Exception;

class RequestManager
{

    private $access_token = '';

    private $refresh_token = '';

    private $auth_code = '';

    private $auth_url = '';

    /**
     * RequestManager constructor.
     * @param $auth_code
     * @param $auth_url
     * @throws Exception
     */
    public function __construct($auth_code, $auth_url)
    {
        $this->auth_code = $auth_code;
        $this->auth_url = preg_replace("/(.*)\/+$/i", "$1", $auth_url);
        $this->set_tokens();
    }


    /**
     * @param $type
     * @param $path
     * @param array $params
     * @param array $header
     * @return bool|mixed
     * @throws Exception
     */
    public function request($type, $path, $params = [], $header = [])
    {
        $ch = curl_init();

        if (false === $ch) {
            throw new Exception('failed to initialize curl');
        }

        $headers = array_filter(array_merge(array(
            'Accept: application/json',
            'access-token: ' . $this->access_token
        ), $header));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        if ($params) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        curl_setopt($ch, CURLOPT_URL, $this->auth_url . $path);


        $content = curl_exec($ch);

        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($content) {
            $res = json_decode($content);
        }

        if (isset($res->errors)) {
            throw new Exception(implode(";", (array)$res->errors));
        }

        if((int)$code >= 300) {
            throw new Exception("Server response error with http code: " . $code);
        }

        return (isset($res) && $res) ? $res : $content;
    }

    /**
     * @throws Exception
     */
    private function set_tokens()
    {
        $state = rand(0, 999999);

        $headers = array(
            'Accept: application/json',
            'auth-code: ' . $this->auth_code,
            'state: ' . $state,
        );

        $c = curl_init();

        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($c, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($c, CURLOPT_URL, $this->auth_url . '/restauth/verify');

        $content = curl_exec($c);

        curl_close($c);

        $obj = json_decode($content);

        if (isset($obj->errors)) {
            throw new Exception(implode(',', $obj->errors));
        }

        if (!isset($obj->state)) {
            $this->error = 'state not defined';
            throw new Exception('state not defined');
        } else {
            if ($state != $obj->state) {
                $this->error = 'state not equal';
                throw new Exception('state not equal');
            }
        }

        if (!isset($obj->access_token)) {
            $this->error = 'no access token';
            throw new Exception('No access-token');
        } else {
            $this->access_token = $obj->access_token;
        }

        if (isset($obj->refresh_token)) {
            $this->refresh_token = $obj->refresh_token;
        }
    }
}
