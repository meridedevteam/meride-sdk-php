## EmbedManager examples
#### Connection to Meride
```php
use Meride\Rest\RequestManager;

use Meride\Rest\EmbedManager;
use Meride\Rest\VideoManager;

$auth_code = "AUTH_CODE";
$auth_url = "URL";

/**
 *  Connection to platform
 */

try {

    $connection = new RequestManager($auth_code, $auth_url);
} catch (\Exception $e) {
    echo $e->getMessage();
}

$embedManager = new EmbedManager($connection);
$videoManager = new VideoManager($connection);
```

#### Search embeds
```php
/**
 * Search for embeds in platform
 *
 * Without any parameters the method will return all embeds
 */
try {
    echo print_r($embedManager->search([
        "search_string" => "test"
    ]), true);
} catch (Exception $e) {
    echo $e->getMessage();
}
```

#### Get embed
```php
/**
 * Get embed by id
 *
 */
$id = 1000;

try {
    echo print_r($embedManager->get($id), true);
} catch (Exception $e) {
    echo $e->getMessage();
}
```

#### Create new embed
```php
/**
 * Create new embed element with essential parameters
 */
$id_video = 1000;

    try {
        //first of all check if the video exist, encoded and ready
        $video = $videoManager->get($id_video);

        if ($video && $video->video_disponibile) {
        
            //if video is ok let's create new embed
            $params = array(
                "titolo" => "Test embed meride sdk " . rand(0, 999),
                "descrizione" => "Test description embed meride sdk " . rand(0, 999),
                "video_id" => $id_video
            );

            try {
                echo print_r($embedManager->add($params), true);
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }

```
#### Edit embed
```php
/**
 * Edit embed by id
 */
 
 $id_embed = 1000;
 
$params = array(
        'titolo' => "Test change title"
    );

    try {
        echo print_r($embedManager->edit($id_embed, $params), true);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
```

#### Delete embed

```php
/**
 * Delete embed by id
 */

$id = 1000;

try {
    echo print_r($embedManager->delete($id), true);
} catch (Exception $e) {
    echo $e->getMessage();
}

```