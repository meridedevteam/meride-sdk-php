## VideoManager examples
#### Connection to Meride
```php
use Meride\Rest\RequestManager;

use Meride\Rest\VideoManager;

$auth_code = "AUTH_CODE";
$auth_url = "URL";

/**
 *  Connection to platform
 */

try {

    $connection = new RequestManager($auth_code, $auth_url);
} catch (\Exception $e) {
    echo $e->getMessage();
}

$videoManager = new VideoManager($connection);
```

#### Search videos
```php
/**
 * Search for videos in platform
 *
 * Without any parameters the method will return all videos
 */
try {
    echo print_r($videoManager->search([
        "search_string" => "test"
    ]), true);
} catch (Exception $e) {
    echo $e->getMessage();
}
```

#### Get video
```php
/**
 * Get video by id
 *
 */
$id = 1000;

try {
    echo print_r($videoManager->get($id), true);
} catch (Exception $e) {
    echo $e->getMessage();
}
```

#### Create new video
```php
/**
 * Create new video element with essential parameters
 */
$params = array(
    'titolo' => "Test caricamento da sdk " . rand(0, 999),
    'video' => "PATH_OF_VIDEO_FILE",
    'descrizione' => "Descrizione di test da sdk " . rand(0, 999),
    'descrizione_breve' => "Descrizione di test da sdk " . rand(0, 999),
    'snapshot' => '1',
    'snapshot_time' => '00:00:01.000',
);

try {
    echo print_r($videoManager->add($params), true);
} catch (Exception $e) {
    echo $e->getMessage();
}

```
#### Edit video
```php
/**
 * Edit video by id
 */
 
 $id_video = 1000;
 
$params = array(
        'titolo' => "Test change title"
    );

    try {
        echo print_r($videoManager->edit($id_video, $params), true);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
```

#### Delete video

```php
/**
 * Force delete video by id
 *
 * Pay attention, this method will delete the video only if it has no associated embed
 */

$id = 1000;

try {
    echo print_r($videoManager->delete($id), true);
} catch (Exception $e) {
    echo $e->getMessage();
}

```

```php
/**
 * Delete video by id
 *
 * This method first of all will delete all embed associated after that will remove video element
 */

$id = 1000;

try {
    echo print_r($videoManager->forceDeleteVideo($id), true);
} catch (Exception $e) {
    echo $e->getMessage();
}
```