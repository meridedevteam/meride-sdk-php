Meride Rest, PHP client rest manager
=======================
## How to use it
First you need to instantiate the RequestManager class passing as parameters in the auth_code and auth_url constructor as in the example:

```php
use Meride\Rest\RequestManager;

$auth_code = "AUTH_CODE";
$auth_url = "URL";

try {
    /**
     *  Connection to platform
     *
     */
    $connection = new RequestManager($auth_code, $auth_url);
} catch (\Exception $e) {
    echo $e->getMessage();
}
```

After that is possible inject RequestManager instance into a some manager as this example:

```php
use Meride\Rest\VideoManager;

$videoManager = new VideoManager($connection);


    /**
     * Search videos in platform
     *
     * Whitout any parameters the method will return all videos
     */
    try {
        echo print_r($videoManager->search([
            "search_string" => "test"
        ]), true);
    } catch (Exception $e) {
        echo $e->getMessage();
    }
```

## Managers
Each manager manage api request for a module of **Meride**. List of manager avaliables:

* VideoManager
* EmbedManager
* SocialManager


## Help and docs api parameters

- [Documentation](http://www.meride.tv/docs/api/api/v0-12/presentazione_generale)